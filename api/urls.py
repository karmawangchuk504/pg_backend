from django.urls import path
from . import views


urlpatterns = [
    path('CandidateData/', views.getCandidateData),
    path('CandidateDataCreate/', views.CandidateDataCreate),
    path('CandidateData/<str:id>', views.CandidateDataUpdate),
    path('CandidateDataDelete/<str:id>', views.CandidateDataDelete),

    path('CandidateInterview/', views.getCandidateInterview),
    path('CandidateInterviewCreate/', views.CandidateInterviewCreate),
    path('CandidateInterview/<str:id>', views.CandidateInterviewUpdate),
    path('CandidateInterviewDelete/<str:id>', views.CandidateInterviewDelete)
]