from django.http.response import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.shortcuts import render

from api.models import CandidateData,CandidateInterview
from .serializers import CandidateDataSerializer,CandidateInterviewSerializer
# Create your views here.
# --------------------------- Canadidate Detals ------------------------------------------------#
@api_view(['GET'])
def getCandidateData(request):
    candidateData = CandidateData.objects.all()
    serialize = CandidateDataSerializer(candidateData, many=True)
    return Response(serialize.data)

@api_view(['POST'])
def CandidateDataCreate(request):
    serialize = CandidateDataSerializer(data=request.data)
    
    if serialize.is_valid():
        serialize.save()
        
        return Response(serialize.data)

    else:
        return Response("Error")

@api_view(['POST'])
def CandidateDataUpdate(request,id=id):
    candidateData = CandidateData.objects.get(id=id)

    serialize = CandidateDataSerializer(instance=candidateData, data=request.data)
    
    if serialize.is_valid():
        serialize.save()
        
        return Response(serialize.data)

    else:
        return Response("Error")

@api_view(['DELETE'])
def CandidateDataDelete(request,id=id):
    candidateData = CandidateData.objects.get(id=id)
    candidateData.delete()
    
    return Response("Data Deleted")
# --------------------------- Canadidate Detals ------------------------------------------------#


# --------------------------- Canadidate Interview ------------------------------------------------#
@api_view(['GET'])
def getCandidateInterview(request):
    candidateData = CandidateInterview.objects.all()
    serialize = CandidateInterviewSerializer(candidateData, many=True)
    return Response(serialize.data)

@api_view(['POST'])
def CandidateInterviewCreate(request):
    serialize = CandidateInterviewSerializer(data=request.data)
    
    if serialize.is_valid():
        serialize.save()
        
        return Response(serialize.data)

    else:
        return Response("Error")

@api_view(['POST'])
def CandidateInterviewUpdate(request,id=id):
    candidateData = CandidateInterview.objects.get(id=id)

    serialize = CandidateInterviewSerializer(instance=candidateData, data=request.data)
    
    if serialize.is_valid():
        serialize.save()
        
        return Response(serialize.data)

    else:
        return Response("Error")

@api_view(['DELETE'])
def CandidateInterviewDelete(request,id=id):
    candidateData = CandidateInterview.objects.get(id=id)
    candidateData.delete()
    
    return Response("Data Deleted")
# --------------------------- Canadidate Interview ------------------------------------------------#