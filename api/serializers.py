from rest_framework import serializers
from .models import CandidateData, CandidateInterview

class CandidateDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = CandidateData
        fields = '__all__'

class CandidateInterviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = CandidateInterview
        fields = '__all__'