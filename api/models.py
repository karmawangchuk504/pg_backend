from django.db import models

# Create your models here.
class CandidateData(models.Model):

    candidate_name = models.TextField()
    gender = models.CharField(max_length=10)
    email = models.EmailField()
    profile_on_sunrise = models.TextField()
    dob = models.TextField()
    country = models.TextField()
    registeration_date = models.TextField()
    job_applied = models.TextField()
    education = models.TextField()
    jlpt = models.TextField()
    employment_status = models.IntegerField()
    employment_date = models.TextField()
    work_experience = models.IntegerField()
    visa_date = models.TextField()
    remark = models.TextField()
    resume_link = models.TextField()

    def __str__(self):
        return self.candidate_name

class CandidateInterview(models.Model):

    cd_id = models.IntegerField()
    hiring_company = models.TextField()
    interview_date = models.TextField()
    result = models.TextField()
    reason = models.TextField()
    end_date = models.TextField()
    note = models.TextField()

    def __str__(self):
        return self.cd_id

        
