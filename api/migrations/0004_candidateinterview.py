# Generated by Django 3.2.7 on 2021-10-01 11:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_auto_20211001_0315'),
    ]

    operations = [
        migrations.CreateModel(
            name='CandidateInterview',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cd_id', models.IntegerField()),
                ('hiring_company', models.TextField()),
                ('interview_date', models.TextField()),
                ('result', models.TextField()),
                ('reason', models.TextField()),
                ('end_date', models.TextField()),
                ('note', models.TextField()),
            ],
        ),
    ]
